public class Main {

    public static void main(String[] args) {

        String method = args[0];
        int num1 = Integer.parseInt(args[1]);
        int num2 = Integer.parseInt(args[2]);
        int result = calc(method, num1, num2);
        System.out.println(result);

        //System.out.println("Hello World");
    }

    private static int calc(String method, int num1, int num2) {
        int result;

        if (method.equals("plus")) {
            result = num1 + num2;
        } else {
            result = 0;
        }

        return result;
    }
}
